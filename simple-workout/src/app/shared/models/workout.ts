import { BaseModel } from './base.model';
import { Exercise } from './exercise';

export class Workout extends BaseModel {
  name: string = '';
  excerciseIds: number[] = [];
  excercises: Exercise[] = [];
}
