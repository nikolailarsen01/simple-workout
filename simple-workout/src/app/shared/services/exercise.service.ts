import { Injectable } from '@angular/core';
import { BaseService } from './base/base.service';
import { Exercise } from '../models/exercise';

@Injectable({
  providedIn: 'root',
})
export class ExerciseService extends BaseService<Exercise> {
  constructor() {
    super();
    this.storageKey = 'exercises';
  }

  listSpecific(ids: number[]) {
    const exercises = this.getItems();
    let returnExercises: Exercise[] = [];
    ids.forEach((x) => {
      returnExercises.push(exercises.find((y) => (y.id = x))!);
    });
    return exercises;
  }
}
