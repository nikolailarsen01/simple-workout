import { Injectable } from '@angular/core';
import { BaseModel } from '../../models/base.model';

@Injectable({
  providedIn: 'root',
})
export class BaseService<T extends BaseModel> {
  protected storageKey!: string;
  constructor() {}

  get(id: number) {
    return this.getItems().find((x) => x.id);
  }

  getItems() {
    let itemsRaw = localStorage.getItem(this.storageKey);
    let items: T[] = [];
    if (itemsRaw) {
      items = JSON.parse(itemsRaw);
      return items;
    } else return items;
  }
  addItem(item: T) {
    const items = this.getItems();
    item.id = items.length + 1;
    items.push(item);
    this.setItems(items);
  }
  saveItem(item: T) {
    const items = this.getItems();
    items[item.id - 1] = item;
    this.setItems(items);
  }

  protected setItems(items: T[]) {
    localStorage.setItem(this.storageKey, JSON.stringify(items));
  }
}
