import { Injectable } from '@angular/core';
import { Workout } from '../models/workout';
import { BaseService } from './base/base.service';
import { ExerciseService } from './exercise.service';

@Injectable({
  providedIn: 'root',
})
export class WorkoutService extends BaseService<Workout> {
  constructor(private exerciseService: ExerciseService) {
    super();
    this.storageKey = 'workouts';
  }
  saveWorkout(workout: Workout) {
    workout.excercises = [];
    this.saveItem(workout);
  }
  override get(id: number) {
    const workout = super.get(id);

    if (workout) {
      if (workout.excerciseIds.length != 0)
        workout.excercises = this.exerciseService.listSpecific(
          workout.excerciseIds
        );
      return workout;
    } else return workout;
  }
}
