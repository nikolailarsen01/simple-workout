import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { CreateComponent } from './pages/create/create.component';

const routes: Routes = [
  {
    path: 'index', // This represents the base path for the module
    component: IndexComponent,
  },
  {
    path: 'create', // This represents the base path for the module
    component: CreateComponent,
  },

  // Add more routes here if needed
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExerciseRoutingModule {}
