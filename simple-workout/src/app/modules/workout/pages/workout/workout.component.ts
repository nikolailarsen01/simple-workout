import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Workout } from 'src/app/shared/models/workout';
import { WorkoutService } from 'src/app/shared/services/workout.service';

@Component({
  selector: 'app-workout',
  templateUrl: './workout.component.html',
  styleUrls: ['./workout.component.scss'],
})
export class WorkoutComponent implements OnInit {
  workout!: Workout;
  id: number;
  constructor(
    private route: ActivatedRoute,
    private workoutService: WorkoutService
  ) {
    this.id = Number(route.snapshot.paramMap.get('id')!);
  }
  ngOnInit(): void {
    this.workout = this.workoutService.get(this.id)!;
  }
}
