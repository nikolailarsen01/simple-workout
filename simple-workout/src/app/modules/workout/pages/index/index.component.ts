import { Component, OnInit } from '@angular/core';
import { Workout } from 'src/app/shared/models/workout';
import { WorkoutService } from 'src/app/shared/services/workout.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  workouts: Workout[] = [];

  constructor(private workoutService: WorkoutService) {}

  ngOnInit(): void {
    this.workouts = this.workoutService.getItems();
  }
}
