import { Component } from '@angular/core';
import { Workout } from 'src/app/shared/models/workout';
import { WorkoutService } from 'src/app/shared/services/workout.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent {
  name?: string;

  constructor(private workoutService: WorkoutService) {}

  createWorkout() {
    if (this.name) {
      const workout = new Workout();
      workout.name = this.name;
      this.workoutService.saveWorkout(workout);
    }
  }
}
