import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './pages/create/create.component';
import { WorkoutRoutingModule } from './workout.routing';
import { IndexComponent } from './pages/index/index.component';
import { FormsModule } from '@angular/forms';
import { WorkoutComponent } from './pages/workout/workout.component';

@NgModule({
  declarations: [CreateComponent, IndexComponent, WorkoutComponent],
  imports: [CommonModule, WorkoutRoutingModule, FormsModule],
  exports: [],
})
export class WorkoutModule {}
