import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './pages/create/create.component';
import { IndexComponent } from './pages/index/index.component';
import { WorkoutComponent } from './pages/workout/workout.component';

const routes: Routes = [
  {
    path: '', // This represents the base path for the module
    component: IndexComponent,
  },
  {
    path: 'new',
    component: CreateComponent,
  },
  {
    path: ':id',
    component: WorkoutComponent,
  },
  // Add more routes here if needed
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkoutRoutingModule {}
