import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'workout',
    loadChildren: () =>
      import('src/app/modules/workout/workout.module').then(
        (m) => m.WorkoutModule
      ),
  },
  {
    path: 'exercise',
    loadChildren: () =>
      import('src/app/modules/exercise/exercise.module').then(
        (m) => m.ExerciseModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
